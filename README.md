# M07 Guestbook Example

A simple guest book using Node, Express, BootStrap, EJS

## How to use

Open a command window in your c:....\44563\w07 folder.

Run npm install to install all the dependencies in the package.json file.

Run node gbapp.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node gbapp.js
```
You can install nodemon package and run all Node.js apps using it.
This would reload the app automatically on making any changes to the source code.
...Install nodemon...
node install -g nodemon
......
...Run using nodemon...
nodemon gbapp
......

Point your browser to `http://localhost:8081`.